<?php

namespace ApiServer\Modules;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function register()
    {
        //register all the service provider this module needs
        $this->app->register('ApiServer\Modules\Providers\AuthServiceProvider');
    }

    public function boot()
    {
        require __DIR__ . '/Http/routes.php';

        //register database migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
}

?>

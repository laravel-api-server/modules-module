<?php

namespace ApiServer\Modules\Policies;

use ApiServer\Core\Policies\BasePolicy;
use ApiServer\Core\Models\User;
use ApiServer\Modules\Models\Module;

class ModulePolicy extends BasePolicy
{
    public function index(User $authUser) {
      return $this->checkPermissions($authUser, 'index', 'module');
    }

    public function store(User $authUser, Module $module = null) {
      return $this->checkPermissions($authUser, 'store', 'module');
    }

    public function show(User $authUser, Module $module) {
      return $this->checkPermissions($authUser, 'show', 'module', $module);
    }

    public function update(User $authUser, Module $module) {
      return $this->checkPermissions($authUser, 'update', 'module', $module);
    }

    public function destroy(User $authUser, Module $module) {
      return $this->checkPermissions($authUser, 'destroy', 'module', $module);
    }
}

<?php

Route::group([
    'namespace' => '\ApiServer\Modules\Http\Controllers',
    'middleware' => 'cors'], function()
{
	/**
	 * Routes using JWT auth
	 */
	Route::group(['middleware' => ['auth.jwt']], function () {
        Route::resource('modules', ModulesController::class, ['only' => [
			    'index', 'show', 'store', 'update', 'destroy'
		]]);
	});
});

?>

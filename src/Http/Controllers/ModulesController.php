<?php

namespace ApiServer\Modules\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

class ModulesController extends DefaultResourceController
{
    public function model() {
      return 'ApiServer\Modules\Models\Module';
    }

    public function serializer() {
        return 'ApiServer\Modules\Serializers\ModuleSerializer';
    }

    public function resource() {
        return "modules";
    }
}

<?php
/**
 * This file contains the User model class.
 */

namespace ApiServer\Modules\Models;

use ApiServer\Core\Models\BaseModel;

class Module extends BaseModel
{
    /**
    * Bootstrap any application services.
    */
    public static function boot()
    {
      parent::boot();

      //Register validation service
      self::creating(
          function ($model) {
              return $model->validate([
                  'name' => '',
                  'provider' => ''
              ]);
          }
      );

      self::updating(
          function ($model) {
              return $model->validate([
                  'name' => $model->name,
                  'provider' => $model->provider
              ]);
          }
      );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'provider', 'enabled'];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
            'name' => 'required|between:1,200|unique:modules,name,{id}',
            'provider' => 'required|between:1,200|unique:modules,provider,{id}',
            'enabled' => 'required|boolean'
    ];
}

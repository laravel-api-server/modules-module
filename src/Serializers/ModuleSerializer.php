<?php

namespace ApiServer\Modules\Serializers;

use ApiServer\Modules\Models\Module;
use ApiServer\JsonApi\Serializers\BaseSerializer;

class ModuleSerializer extends BaseSerializer
{
    protected $type = 'modules';

    public function getAttributes($module, array $fields = null)
    {
        if (! ($module instanceof Module)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.Module::class
            );
        }

        return [
            'name' => $module->name,
            'provider'  => $module->provider,
            'enabled'  => $module->enabled,
            'created_at'  => $this->formatDate($module->created_at),
            'updated_at' => $this->formatDate($module->updated_at)
        ];
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/modules/{$model->id}",
        ];

        //links to include based permissions
        if(\Gate::allows('show', $model))
            $links['read'] = config('app.url')."/modules/{$model->id}";
        if(\Gate::allows('update', $model))
            $links['update'] = config('app.url')."/modules/{$model->id}";
        if(\Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/modules/{$model->id}";

        return $links;
    }
}

?>

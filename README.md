# Modules Module for the API-Server
This project holds the Modules module for the API-Server. It supports the modularization of the server.

## Installation
This module is hardcoded into the server.

## Depencies
 * Authorization
 * Users
 * ApiServer\JsonApi
 * Uuid

## Provides
 * BaseModel
 * BaseSerializer

## Documentation
 * [Technology stack](doc/technology.md)
 * [Unittesting](doc/unittesting.md)
 * [Creating a release](doc/release.md)

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

# License
See [License](LICENSE.txt)
